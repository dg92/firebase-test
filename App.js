/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Clipboard} from 'react-native';
import firebase from 'react-native-firebase';

const androidConfig = {
  clientId: '525755783081-hbougc355ti8ve1ob44qckd35k4sunnr.apps.googleusercontent.com',
  appId: '1:525755783081:android:c045701534b0f96a',
  apiKey: 'AIzaSyBXS-Ws5wjD1v2mzPT_WjOrpiUPfN3YWIc',
  databaseURL: 'x',
  storageBucket: 'ethos-test2.appspot.com',
  messagingSenderId: 'x',
  projectId: 'ethos-test2',

  // enable persistence by adding the below flag
  persistence: true,
};

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor (props) {
    super(props);
    this.state = {
      test: null
    };
  }

  componentDidMount() {
    firebase.messaging().getToken()
        .then(fcmToken => {
        this.setState({test: 'token -> ' + fcmToken})
      }).catch(e => {
        this.setState({test: "token" + JSON.stringify(e, 4, null)})
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>{this.state.test}</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
